# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien

_Une phrase en italique_

__Une phrase en gras__

Un lien vers [fun-mooc.fr](fun-mooc.fr)

Une ligne de `code`

## Sous-partie 2 : listes
__Liste à puces__

* item
  * sous-item
  * sous-item
* item
* item

__Liste numérotée__

1. item
1. item
1. item

## Sous-partie 3 : code

```python
 # Extrait de code
```

